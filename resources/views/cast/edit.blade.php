@extends('layout.castTemplate')

@section('title')
Ubah Data
@endsection

@section('content')

<div>
    <h2>Edit Data Cast {{$data->id}}</h2>
    <form action="/cast/{{$data->id}}" method="POST">
        @csrf
        @method('PUT')
        <div class="form-group">
            {{-- name --}}
            <label for="name">Name : </label>
            <input type="text" class="form-control" name="name" value="{{$data->name}}" id="name" placeholder="Masukkan Nama">
            @error('name')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror

            {{-- slug --}}
            <label for="slug">Slug : </label>
            <input type="text" class="form-control" name="slug" value="{{$data->slug}}" id="slug" placeholder="Masukkan Slug">
            @error('slug')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror

            {{-- address --}}
            <label for="address">Address : </label>
            <input type="text" class="form-control" name="address" value="{{$data->address}}" id="address" placeholder="Masukkan Alamat">
            @error('address')
                <div class="alert alert-danger">
                    {{ $message }}
                </div>
            @enderror
        </div>
        <button type="submit" class="btn btn-primary">Edit</button>
    </form>
</div>

@endsection