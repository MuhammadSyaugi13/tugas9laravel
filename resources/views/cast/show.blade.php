@extends('layout.castTemplate')

@section('title')
    Tambah Data
@endsection

@section('content')
<h2>Detail Data</h2>
<ul class="list-group list-group-horizontal">
    <li class="list-group-item"><b>Name :</b></li>
    <li class="list-group-item">{{$data['name']}}</li>
</ul>
<ul class="list-group list-group-horizontal-sm">
    <li class="list-group-item"><b>Slug :</b></li>
    <li class="list-group-item">{{$data['slug']}}</li>
</ul>
<ul class="list-group list-group-horizontal-sm">
    <li class="list-group-item"><b>Address :</b></li>
    <li class="list-group-item">{{$data['address']}}</li>
</ul>
@endsection