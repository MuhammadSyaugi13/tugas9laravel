@extends('layout.castTemplate')

@section('title')
    Index
@endsection

@section('content')

{{-- button --}}
<div class="btn-group" role="group" aria-label="Basic example">
    <button type="button" class="btn btn-primary"><a href="{{url('cast/create')}}">Create</a></button>
    <button type="button" class="btn btn-warning"><a href="{{url('cast/create')}}"></a></button>
    <button type="button" class="btn btn-primary">Right</button>
</div>
{{-- last button --}}

<table class="table">
  <thead>
    <tr>
      <th scope="col">Np</th>
      <th scope="col">Name</th>
      <th scope="col">Slug</th>
      <th scope="col">Address</th>
      <th scope="col">Aksi</th>
    </tr>
  </thead>
  <tbody>
      @php
          $no = 1;
      @endphp
      @foreach ($datas as $data)
        <tr>
            <td>{{$no}}</td>
            <td>{{$data['name']}}</td>
            <td>{{$data['slug']}}</td>
            <td>{{$data['address']}}</td>
            <td>
                <button type="button" class="btn btn-sm btn-success"><a href="{{url('cast/'.$data['id'])}}">Detail</a></button>
                <button type="button" class="btn btn-sm btn-warning"><a href="{{url('cast/'.$data['id'].'/edit')}}">Edit</a></button>
                <form action="{{url('cast/'.$data['id'])}}">
                  @csrf
                  @method('delete')
                  <button type="submit" class="btn btn-sm btn-danger">Delete</button>
                </form>
            </td>
        </tr>

        @php
            $no++;
        @endphp
      @endforeach
    
  </tbody>
</table>

@endsection