@extends('layout.castTemplate')

@section('title')
    Tambah Data
@endsection

@section('content')

{{-- button --}}
<div class="btn-group" role="group" aria-label="Basic example">
    <button type="button" class="btn btn-primary"><a href="{{url('cast')}}">Back To Index</a></button>
</div>
{{-- last button --}}


{{-- form --}}

<form action="{{url('cast')}}" method="POST">
  @csrf
  <div class="mb-3">
    <label for="InputName" class="form-label">Name :</label>
    <input type="text" name="name" class="form-control" id="InputName" >
  </div>

  <div class="mb-3">
    <label for="InputSlug" class="form-label">Slug :</label>
    <input type="text" name="slug" class="form-control" id="InputSlug" >
  </div>

  <div class="mb-3">
    <label for="InputAddress" class="form-label">Address :</label>
    <input type="text" name="address" class="form-control" id="InputAddress" >
  </div>

  <button type="submit" class="btn btn-primary">Submit</button>
</form>

{{-- last form --}}

@endsection