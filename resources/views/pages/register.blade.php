@extends('layout.master')

@section('title')
    Register
@endsection

@section('content')

<h2>Halaman Register</h2>
    
<!-- Default box -->
      <div class="card mt-5">
        <div class="card-header">
          <h3 class="card-title">Halaman Register</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">

          
          <h2>Buat Account Baru</h2>
          <h5>Sign Up Form</h5>

          <form action="register" method="post">
              @csrf
            <!-- input first name -->
            <label for="firstName">
              <p>First Name :</p>
            </label>
            <input type="text" name="firstName" id="firstName" />

            <br />
            <!-- input last name -->
            <label for="lastName">
              <p>Last Name :</p>
            </label>
            <input type="text" name="lastName" id="lastName" />

            <!-- input gender -->
            <p>Gender :</p>

            <input type="radio" name="gender" id="male" />
            <label for="male">Male</label>
            <br />
            <input type="radio" name="gender" id="female" />
            <label for="female">Female</label>

            <!-- input nationaly -->
            <label for="nationaly">
              <p>Nationaly</p>
            </label>
            <select name="nationaly" id="nationaly">
              <option value="indonesia">Indonesia</option>
              <option value="amerika">Amerika</option>
              <option value="inggris">Inggris</option>
            </select>

            <!-- input spoke -->
            <p>Language Spoken</p>

            <input type="checkbox" name="bahasaIndonesia" id="BI" />
            <label for="BI">Bahasa Indonesia</label>

            <input type="checkbox" name="english" id="EN" />
            <label for="EN">English</label>

            <input type="checkbox" name="other" id="O" />
            <label for="O">Other</label>

            <label for="bio"><p>Bio</p></label>
            <textarea name="bio" id="bio" cols="10" rows="8"></textarea>

            <br />
            <button type="submit">Sign Up</button>
          </form>

          <br /><br />


        </div>
      </div>
      <!-- /.card -->

  </body>
</html>

@endsection