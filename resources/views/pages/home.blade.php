@extends('layout.master')

@section('title')
    Home
@endsection

@section('content')

<h2>Halaman Home</h2>
    
<!-- Default box -->
      <div class="card mt-5">
        <div class="card-header">
          <h3 class="card-title">Halaman Home</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">

          <h1>Media Online</h1>
          
            <b>Sosial Media Developer</b>
            <p>Belajar dan berbagi agar hidup menjadi lebih baik</p>

            <b>Benefit join di media online</b>
            <ul>
              <li>Mendapatkan motivasi dari sesama developer</li>
              <li>Sharing knowledge</li>
              <li>Dibuat oleh calon web developer terbaik</li>
            </ul>

            <b>Cara bergabung ke Media Online</b>
            <ol>
              <li>Mengunjugin website ini</li>
              <li>Mendaftarkan di <a href="/register">Form Sign Up</a></li>
              <li>Selesai</li>
            </ol>
        </div>
      </div>
      <!-- /.card -->

  </body>
</html>

@endsection