@extends('layout.master')

@section('title')
    Index
@endsection

@section('content')

<h2>Halaman Index</h2>
    
<!-- Default box -->
      <div class="card mt-5">
        <div class="card-header">
          <h3 class="card-title">Halaman Index</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">

          <h1>Selamat Datang {{$data['firstName']}} {{$data['lastName']}}</h1>
          <p>
            <b
              >Terima kasih telah bergabung di website kami. Media Belajar Kita
              bersama!</b>
          </p>
        </div>
      </div>
      <!-- /.card -->

  </body>
</html>

@endsection
