<?php

namespace App\Http\Controllers;

use App\Cast;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CastController extends Controller
{
    // index
    public function index()
    {
        $datas = Cast::all();
        return view('cast.index', $datas);
    }

    // create
    public function create()
    {
    	return view('cast.create');
    }
 
    public function store(Request $request)
    {
    	$this->validate($request,[
    		'name' => 'required',
    		'slug' => 'required',
    		'address' => 'required'
    	]);
 
        Cast::create([
    		'name' => $request->name,
    		'slug' => $request->slug,
       		'adress' => $request->address
    	]);
 
    	return redirect('/cast');
    }

    // show
    public function show($id)
    {
        $data = Cast::find($id);
        return view('cast.show', $data);
    }

    // change data
    public function edit($id)
    {
        $data = Post::find($id);
        return view('cast.edit', $data);
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'name' => 'required',
            'slug' => 'required',
            'address' => 'required',
        ]);

        $data = Cast::find($id);
        $data->name = $request->name;
        $data->slug = $request->slug;
        $data->address = $request->address;
        $data->update();
        return redirect('/cast');
    }

    // delete


    public function destroy($id)
    {
        $cast = Cast::find($id);
        $cast->delete();
        return redirect('/cast');
    }
}
